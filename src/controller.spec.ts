import { assert } from 'chai';
import { DefaultProportional, DefaultIntegral, DefaultDifferential, Controller } from "./controller";

describe("Controller", () => {
	describe("Constructor", () => {
		it("Starts with default values", async () => {
			let controller = new Controller();
			assert.strictEqual(controller.Proportional, DefaultProportional);
			assert.strictEqual(controller.Integral, DefaultIntegral);
			assert.strictEqual(controller.Differential, DefaultDifferential);
		})
		it("Respects input values", async () => {
			const [P, I, D] = [9, 8, 7];
			let controller = new Controller(P, I, D);
			assert.strictEqual(controller.Proportional, P);
			assert.strictEqual(controller.Integral, I);
			assert.strictEqual(controller.Differential, D);
		});
	});
});