export const DefaultProportional = 1;
export const DefaultIntegral = 0.5;
export const DefaultDifferential = 0;

export class Controller {
	public constructor(P?: number, I?: number, D?: number) {
		this.Proportional = P ?? this.Proportional;
		this.Integral = I ?? this.Integral;
		this.Differential = D ?? this.Differential;
	}
	/** Default 1 */
	public Proportional: number = DefaultProportional;
	/** Default 0.5 */
	public Integral: number = DefaultIntegral;
	/** Default 0 */
	public Differential: number = DefaultDifferential;
	private lastError?: number;
	private totalError: number = 0;
	public Tune(P?: number, I?: number, D?: number) {
		this.Proportional = P ?? this.Proportional;
		this.Integral = I ?? this.Integral;
		this.Differential = D ?? this.Differential;
	}
	/** Calculate and return the next correction
	 * 
	 * @param error The error to correct
	 * @param dT The time passed since the last correction
	 */
	public Correct(error: number, dT: number): number {
		// Init the correction value
		let correction = 0;

		/** Prop */
		// Calculate proportional correction
		correction += this.Proportional * error;

		/** Diff */
		// Calculate difference in error since the last correction
		const dError = this.lastError === undefined ? 0 : error - this.lastError;
		// Record this error for next correction
		this.lastError = error;
		// Calculate diff correction
		correction += this.Differential * (dError / dT);

		/** Int */
		// Add to total error recorded to date
		this.totalError += error;
		// Calculate the integral correction
		correction += this.Integral * this.totalError;

		return correction;
	}
}

export default Controller;